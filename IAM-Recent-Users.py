import boto3

# Establish connection to the IAM service
iam_client = boto3.client('iam', region_name="eu-west-2")

# Retrieve all IAM users
users = iam_client.list_users()

# Iterate through each IAM user
for user in users['Users']:
    user_name = user['UserName']

    # Extract PasswordLastUsed attribute (if available)
    last_activity = user.get('PasswordLastUsed')

    # Print user name
    print(user_name)

    # Check if PasswordLastUsed attribute exists
    if last_activity:
        print(last_activity)
    else:
        print("No activity recorded")