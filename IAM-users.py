import boto3

# Establish connection to the IAM service
iam_client = boto3.client('iam', region_name="eu-west-2")

users = iam_client.list_users()

for user in users['Users']:
    user_name = user['UserName']
    print(user_name)

