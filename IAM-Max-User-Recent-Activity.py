import boto3

# Establish connection to the IAM service

iam_client = boto3.client('iam', region_name="eu-west-2")

# Retrieve all IAM users
users = iam_client.list_users()

# Get the maximum last_activity time, excluding None values
max_last_activity_time = max([user.get('PasswordLastUsed')
     for user in users['Users']
     if user.get('PasswordLastUsed') is not None])

# Find the user associated with the maximum last_activity time
most_recently_active_user = next(user for user in users['Users'] if user.get('PasswordLastUsed') == max_last_activity_time)

# Extract user ID and name
user_id = most_recently_active_user['UserId']
user_name = most_recently_active_user['UserName']

# Print the user ID and name of the most recently active user
print(f"Most recent max activity time: {max_last_activity_time}")
print(f"Most recent active UserID: {user_id}")
print(f"Most recent active User: {user_name}")