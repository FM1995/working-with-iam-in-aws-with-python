# Working with IAM in AWS

#### Project Outline

In this project we will utilise Python and AWS to achieve the below

•	Get all the IAM users in your AWS account


•	For each user, print out the name of the user and when they were last active


•	Print out the user ID and name of the user who was active the most recently

#### Lets get started

Using the below documentation can start off by addressing the above


https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam/client/list_users.html

##### Get all the IAM users in your AWS account

Using the above documentation can configure the below code

![Image1](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image1.png)

Cross checking with console

![Image2](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image2.png)

##### For each user, print out the name of the user and when they were last active

Below is the finalised code to get the username and last recorded activity

![Image3](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image3.png)

##### Print out the user ID and name of the user who was active the most recently

Can first get the max last activity time for the last logged in users using the ‘PasswordLastUsed’ attribute

![Image4](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image4.png)

Can then iterate though to get the user who has the max last user activity

![Image5](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image5.png)

Can then print

![Image6](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image6.png)

Can then extract the User details

![Image7](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image7.png)

Can then print it

![Image8](https://gitlab.com/FM1995/working-with-iam-in-aws-with-python/-/raw/main/Images/Image8.png)


